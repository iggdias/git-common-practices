# git-common-practices

* Ignorar, por padrão:
 
  * /.classpath
  * /.settings
  * /target/
  * /*.project
  * /*.springBeans

* Se necessário, remover arquivos do index com

> git rm --cached NOME_DO_ARQUIVO

ou

> git rm --cached NOME_DO_DIRETORIO -r
